extends Node
class_name ColorPalette

export var colors : Dictionary


func get_color(color_name : String) -> Color:
	if colors.has(color_name):
		return colors[color_name]
	return colors["default"]

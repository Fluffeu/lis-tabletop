extends Sprite
class_name AutocolorSprite

export var color_name : String = "default"

onready var palette = TreeUtilities.get_fast("ColorPalette")


func _ready() -> void:
	if palette:
		modulate = palette.get_color(color_name)

tool
extends Node2D
class_name Board

export var tile_size : int = 32
export var row_size : int = 10


func _process(delta : float) -> void:
	if Engine.editor_hint:
		var i : int = 0
		for c in get_children():
			c.position.x = tile_size*(i%row_size)
			c.position.y = tile_size*int(i/row_size)
			if int(c.position.x/tile_size + c.position.y/tile_size) % 2 == 0:
				c.self_modulate = Color(1.0, 1.0, 1.0)
			else:
				c.self_modulate = Color(0.9, 0.9, 0.9)
			i += 1

tool
extends Node2D
class_name BoardElement

export var tile_size : int = 128


func _process(delta : float) -> void:
	if Engine.editor_hint:
		position.x = int(position.x) - int(position.x)%tile_size
		position.y = int(position.y) - int(position.y)%tile_size

extends Node2D
class_name Player

onready var level = TreeUtilities.get_fast("Level")
onready var board = TreeUtilities.get_fast("Board")
onready var camera = TreeUtilities.get_fast("Camera2D")
onready var player_i : int = get_parent().get_children().find(self)

var selected_unit = null
var has_turn : bool = false
var can_deselect : bool = true
var showing_shadow : bool = false


remotesync func change_turn_state(state : bool) -> void:
	has_turn = state


remotesync func change_deselection_state(state : bool) -> void:
	print(name + " changing deselection state to " + str(state))
	can_deselect = state


func mark_selected(unit) -> void:
	if !is_network_master() or selected_unit:
		print("can't select - other unit selected")
		return
	
	print("marking " + unit.name + " as selected")
	showing_shadow = true
	$MoveShadow.texture = unit.get_node("Sprite").texture
	$MoveShadow.position = unit.get_node("Sprite").position
	$MoveShadow.scale = unit.get_node("Sprite").scale
	selected_unit = unit
	level.show_move_indication(unit)


func remove_selection(unit) -> void:
	showing_shadow = false
	if !is_network_master() or !can_deselect:
		return
	
	if selected_unit == unit:
		selected_unit.deselect()
		selected_unit = null
		level.hide_move_indication()


func _input(event : InputEvent) -> void:
	if !is_network_master():
		return
	
	if selected_unit:
		var mouse_pos : Vector2 = get_local_mouse_position()
		var target_pos : Vector2 = mouse_pos + Vector2(board.tile_size/2, board.tile_size/2)
		target_pos = Vector2(
			int(target_pos.x) - int(target_pos.x)%board.tile_size,
			int(target_pos.y) - int(target_pos.y)%board.tile_size
		)
		
		if selected_unit.can_move_to(target_pos):
			$MoveShadow.position = target_pos
			$MoveShadow.show()
	else:
		$MoveShadow.hide()
	
	if Input.is_action_just_pressed("move_select"):
		if selected_unit:
			print("trying to move " + selected_unit.name)
			event = make_input_local(event)
			var click_pos : Vector2 = event.position + Vector2(board.tile_size/2, board.tile_size/2)
			var tx : int = int(click_pos.x) - int(click_pos.x)%board.tile_size
			var ty : int = int(click_pos.y) - int(click_pos.y)%board.tile_size
			var target_position : Vector2 = Vector2(tx, ty)
			if selected_unit.can_move_to(target_position):
				print("moving " + selected_unit.name)
				var item_i : int
				var unit_i : int
				var player_i : int
				
				if selected_unit is Item:
					player_i = selected_unit.owning_unit.owning_player.player_i
					item_i = selected_unit.get_parent().get_children().find(selected_unit)
					unit_i = selected_unit.owning_unit.get_parent().get_children().find(selected_unit.owning_unit)
				else:
					unit_i = selected_unit.get_parent().get_children().find(selected_unit)
					item_i = -1
					player_i = selected_unit.owning_player.player_i
				level.rpc("make_move_intent", player_i, unit_i, target_position, item_i)
				can_deselect = true
				remove_selection(selected_unit)
			elif can_deselect:
				remove_selection(selected_unit)
	
	if Input.is_action_just_pressed("end_turn"):
		level.rpc("make_turn_end_intent", player_i)

extends Node
class_name TurnData

var player : Node = null
var selected_unit : Node = null
var active_units : Array = []
var finish_on_move : bool = false
var replace_movement : bool = false

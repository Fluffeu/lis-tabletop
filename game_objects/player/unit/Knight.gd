extends Unit
class_name Knight

export var move_directions : Array = []
export var move_directions_slowed : Array = []


func can_move_to(target_pos : Vector2) -> bool:
	var possible_moves : Array
	if $Items.get_child_count() > 0:
		possible_moves = move_directions_slowed
	else:
		possible_moves = move_directions
	
	if is_replaced:
		possible_moves = replace_movement
	
	for d in possible_moves:
		if target_pos == position + d*tile_size:
			var targets : Array = level.get_objects_at(target_pos)
			
			for t in targets:
				if t.has_method("block_tile") and t.block_tile(side):
					return false
			
			return true
		
	return false

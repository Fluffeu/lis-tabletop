tool
extends Unit
class_name Cote

export var item_name : String = "ChickenItem"

func interact(object) -> void:
	object.rpc("add_item", item_name)
	rpc("destroy_unit")

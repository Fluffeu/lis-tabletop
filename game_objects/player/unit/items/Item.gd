extends Unit
class_name Item

onready var owning_unit : Node = get_parent().get_parent()


remotesync func update_network_position(_pos : Vector2, can_m : bool) -> void:
	pass


func _input_event(_viewport, event, _shape_idx) -> void:
	if event is InputEventMouseButton and event.pressed:
		if !is_selected and can_move:
			select()
		else:
			deselect()


func move_to(target_pos : Vector2) -> void:
#	print("moving " + name + " to " + str(target_pos))
	for o in level.get_objects_at(target_pos):
		if o.has_method("transfer_item") and o != self:
			var item_i : int = get_parent().get_children().find(self)
			var unit_i : int = owning_unit.get_parent().get_children().find(owning_unit)
			var player_i : int = owning_unit.owning_player.get_parent().get_children().find(owning_unit.owning_player)
			o.rpc("transfer_item", item_i, unit_i, player_i)
			return


func can_move_to(target_pos : Vector2) -> bool:
	if (target_pos - owning_unit.position).length() > owning_unit.tile_size:
		return false
	
	for o in level.get_objects_at(target_pos):
		if o.has_method("transfer_item") and owning_unit.side == o.side:
			return true
	
	return false


func interact(_object) -> void:
	pass

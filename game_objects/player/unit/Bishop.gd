extends Unit
class_name Bishop

export var max_dist : float = 2*sqrt(2)


func can_move_to(target_pos : Vector2) -> bool:
	if is_replaced:
		for d in replace_movement:
			if target_pos == position + d*tile_size:
				var targets : Array = level.get_objects_at(target_pos)
				
				for t in targets:
					if t.has_method("block_tile") and t.block_tile(side):
						return false
				
				return true
		return false
	
	
	if target_pos == position:
		return false
	
	var m_d : float = max_dist
	if $Items.get_child_count() > 0:
		m_d = 1
	var d : Vector2 = target_pos - position
	if d.length() > m_d*tile_size or abs(d.x) != abs(d.y):
		return false
	
	d.x = d.x/abs(d.x)
	d.y = d.y/abs(d.y)
	var i : int = tile_size

	while position + d*i != target_pos:
		var objects : Array = level.get_objects_at(position + d*i)
		for o in objects:
			if o.has_method("block_tile"):
				return false
		i += tile_size

	var objects : Array = level.get_objects_at(position + d*i)
	for o in objects:
		if o.has_method("block_tile") and o.block_tile(side):
			return false
	
	return true

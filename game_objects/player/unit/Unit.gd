tool
extends BoardElement
class_name Unit

export var side : int = 0
export var replace_movement : Array = []

onready var owning_player = get_parent().get_parent()
onready var level = TreeUtilities.get_fast("Level")
onready var palette = TreeUtilities.get_fast("ColorPalette")

var is_selected : bool = false
var can_move : bool = false
var is_replaced : bool = false

signal unit_moved(unit)


func _ready() -> void:
	connect("mouse_entered", self, "_on_mouse_entered")
	connect("mouse_exited", self, "_on_mouse_exited")


remotesync func destroy_unit() -> void:
	queue_free()


remotesync func add_item(item_name : String) -> void:
	var item : Node = load("res://game_objects/player/unit/items/" + item_name + ".tscn").instance()
	item.can_move = true
	$Items.add_child(item)


remotesync func transfer_item(item_i : int, source_unit_i : int, source_player_i : int) -> void:
	var source_unit : Node = level.get_node("Players").get_child(source_player_i).get_node("Units").get_child(source_unit_i)
	var item : Node = source_unit.get_node("Items").get_child(item_i)
	source_unit.get_node("Items").remove_child(item)
	item.owning_unit = self
	$Items.add_child(item)


remotesync func update_network_position(pos : Vector2, can_m : bool) -> void:
	position = pos
	update_activity(can_m)


remotesync func update_network_movability(state : bool, replace_state : bool) -> void:
	update_activity(state)
	is_replaced = replace_state


remotesync func update_network_select_state(state : bool) -> void:
	if state:
		select()
	else:
		deselect()


func _input_event(_viewport, event, _shape_idx) -> void:
	if event is InputEventMouseButton and event.pressed:
		print(name + "clicked. \nSelected: " + str(is_selected) + "\nCan move: " + str(can_move))
		if !is_selected and can_move:
			call_deferred("select")


func _on_mouse_entered() -> void:
	level.call_deferred("show_move_preview", self)


func _on_mouse_exited() -> void:
	level.hide_move_preview()


func select() -> void:
	print("trying to select " + name)
	is_selected = true
	level.get_moving_player().mark_selected(self)


func deselect() -> void:
	is_selected = false


func move_to(target_pos : Vector2) -> void:
#	print("moving " + name + " to " + str(target_pos))
	position = target_pos
	emit_signal("unit_moved", self)
	update_activity(false)
	for o in level.get_objects_at(target_pos):
		if o.has_method("interact") and o != self:
			o.interact(self)


func can_move_to(_target_pos : Vector2) -> bool:
	return false


func interact(object) -> void:
	print(object.name + " interacting with " + name)
	
	for i in range (0, $Items.get_child_count()):
		object.rpc("transfer_item", 0, get_parent().get_children().find(self), owning_player.player_i)
	
	var turn : TurnData = TurnData.new()
	turn.player = object.owning_player
	turn.active_units = [self]
	turn.selected_unit = self
	turn.replace_movement = true
	level.add_child(turn)
	level.add_new_turn(turn)


func block_tile(obj_side : int) -> bool:
	return obj_side == side


func update_activity(state : bool) -> void:
	can_move = state
	if can_move:
		$StatusIcon.modulate = palette.get_color("indication_active")
	else:
		$StatusIcon.modulate = palette.get_color("indication_inactive")

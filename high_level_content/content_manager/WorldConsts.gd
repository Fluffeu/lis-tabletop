extends Node
class_name WorldConsts

export var gravity : Vector2 = Vector2(0.0, 2500.0)
export var ground_friction : float = 10.0
export var air_friction : float = 6.0

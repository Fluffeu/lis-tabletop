extends Node
class_name ContentManager

var current_level : Level


func _ready() -> void:
	randomize()
	load_level("Level01")


func load_level(level_name : String) -> void:
	if current_level:
		current_level.queue_free()
	var new_level : Level = load("res://high_level_content/levels/" + level_name + ".tscn").instance()
	current_level = new_level
	add_child(new_level)

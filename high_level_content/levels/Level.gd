extends Node
class_name Level

var turn_stack : Array = []
var last_true_turn_player : int = -1
var moving_player_i : int = 0
var is_move_indicated : bool = false


remote func make_move_intent(player_i : int, unit_i : int, target_pos : Vector2, item_i : int = -1) -> void:
	print("move intent made")
	print("player: " + str(player_i))
	print("unit: " + str(unit_i))
	print("item: " + str(item_i))
	if !is_network_master() or !is_caller_correct(get_tree().get_rpc_sender_id()):
		return
	
	var object : Node = $Players.get_child(player_i).get_node("Units").get_child(unit_i)
	if item_i > -1:
		object = object.get_node("Items").get_child(item_i)
	
	if !object.can_move_to(target_pos):
		return
	
	var end_turn_immediately : bool = turn_stack.back().selected_unit != null
	
	object.move_to(target_pos)
	object.rpc("update_network_position", target_pos, false)
	
	# automatically end turn after replacing move
	if end_turn_immediately:
		end_turn(turn_stack.back().player)


remote func make_turn_end_intent(player_i : int) -> void:
	if !is_network_master() or !is_caller_correct(get_tree().get_rpc_sender_id()):
		return
	
	print("turn end intent made by player " + str(player_i))
	end_turn($Players.get_child(player_i))


remotesync func update_moving_player(player_i) -> void:
	moving_player_i = player_i


func get_moving_player() -> Node:
	return $Players.get_child(moving_player_i)


func is_caller_correct(sender_id) -> bool:
	#return true
	return sender_id == TreeUtilities.get_fast("Game").sorted_players[moving_player_i]


func _ready() -> void:
	end_turn(null)
	for p in $Players.get_children():
		for u in p.get_node("Units").get_children():
			u.connect("unit_moved", self, "_on_unit_moved")


func _on_unit_moved(unit) -> void:
	turn_stack.back().active_units.erase(unit)


func end_turn(requester) -> void:
	if turn_stack.size() and turn_stack.back().player == requester:
		var turn : TurnData = turn_stack.pop_back()
		turn.queue_free()
	
	if turn_stack.size() == 0:
		var turn : TurnData = TurnData.new()
		last_true_turn_player = (last_true_turn_player + 1)%$Players.get_child_count()
		turn.player = $Players.get_child(last_true_turn_player)
		turn.active_units = turn.player.get_node("Units").get_children()
		add_child(turn)
		add_new_turn(turn)
	else:
		start_top_turn()


func add_new_turn(turn : TurnData) -> void:
	turn_stack.push_back(turn)
	start_top_turn()


func start_top_turn() -> void:
	#TODO: debug from here - starting two turns when replacing
	var turn : TurnData = turn_stack.back()
	print("NEW TURN")
	print("turn replacing state: " + str(turn.replace_movement))
	for p in $Players.get_children():
		p.rpc("change_turn_state", p == turn.player)
		
		for u in p.get_node("Units").get_children():
			u.rpc("update_network_movability", turn.active_units.has(u), turn.replace_movement)
			u.rpc("update_network_select_state", turn.selected_unit == u)
			
			for i in u.get_node("Items").get_children():
				i.rpc("update_network_movability", turn.player == u.owning_player, false)
				i.rpc("update_network_select_state", turn.selected_unit == i)
	
	turn.player.rpc("change_deselection_state", turn.selected_unit == null)
	rpc("update_moving_player", $Players.get_children().find(turn.player))


func get_all_objects() -> Array:
	var ret : Array = []
	ret += $Board.get_children()
	ret += $MiscObjects.get_children()
	for p in $Players.get_children():
		ret += p.get_node("Units").get_children()
	
	return ret


func get_objects_at(pos : Vector2) -> Array:
	var ret : Array = []
	for o in get_all_objects():
		if o.position == pos:
			ret.append(o)
	
	return ret


func show_move_indication(moving_unit) -> void:
	is_move_indicated = true
	for o in get_all_objects():
		if moving_unit.can_move_to(o.position) or o.position == moving_unit.position:
			o.modulate = Color(1.0, 1.0, 1.0)
		else:
			o.modulate = Color(0.45, 0.45, 0.45)


func show_move_preview(previewed_unit) -> void:
	if is_move_indicated:
		return
	for o in get_all_objects():
		if previewed_unit.can_move_to(o.position) or o.position == previewed_unit.position:
			o.modulate = Color(1.3, 1.3, 1.3)
		else:
			o.modulate = Color(0.8, 0.8, 0.8)


func hide_move_indication() -> void:
	is_move_indicated = false
	for o in get_all_objects():
		o.modulate = Color(1.0, 1.0, 1.0)


func hide_move_preview() -> void:
	if is_move_indicated:
		return
	for o in get_all_objects():
		o.modulate = Color(1.0, 1.0, 1.0)

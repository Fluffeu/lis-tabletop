extends Node2D

var sorted_players : Array = []


func _ready():
	print("Entering game")
	get_tree().paused = true
	
	ClientNetwork.connect("remove_player", self, "remove_player")
	
	pre_configure()


func remove_player(playerId: int):
	var playerNode = get_node(str(playerId))
	playerNode.queue_free()


func pre_configure():
	sorted_players = []
	
	for playerId in GameData.players:
		sorted_players.push_back(playerId)
	
	sorted_players.sort()
	
	var i : int = 0
	for playerId in sorted_players:
		assign_player(playerId, i)
		i += 1
	
	if not get_tree().is_network_server():
		# Report that this client is done
		rpc_id(ServerNetwork.SERVER_ID, "on_client_ready", get_tree().get_network_unique_id())


func assign_player(playerId, i):
	print("Assigning player data to game player entity.")
	
	var player = GameData.players[playerId]
	
	if i < TreeUtilities.get_fast("Level").get_node("Players").get_child_count():
		TreeUtilities.get_fast("Level").get_node("Players").get_child(i).set_network_master(playerId, false)
	


remotesync func on_pre_configure_complete():
	print("All clients are configured. Starting the game.")
	get_tree().paused = false

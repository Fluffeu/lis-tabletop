extends Node

var nodes_found : Dictionary = {}


func reset() -> void:
	nodes_found = {}


func remove_from_cache(node_name : String) -> void:
	nodes_found.erase(node_name)


func get_fast(node_name : String) -> Node:
	if !nodes_found.has(node_name) or nodes_found[node_name].get_ref() == null:
		nodes_found[node_name] = weakref(find_node_by_name(node_name))
	return nodes_found[node_name].get_ref()


func find_node_by_name(node_name : String) -> Node:
	print_debug("Searching for node: " + node_name)
	var result : Node = _find_node_by_name(node_name, get_tree().get_root())
	if !result:
		print_debug("Node: " + node_name + " not found!")
	return result


func _find_node_by_name(node_name : String, node : Node) -> Node:
	if node.name.find("*") != -1:
		return null
	if node.name == node_name:
		return node
	for c in node.get_children():
		var result = _find_node_by_name(node_name, c)
		if result != null:
			return result
	return null

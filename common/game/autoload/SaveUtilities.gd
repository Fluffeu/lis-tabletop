extends Node

const registered_files : Dictionary = {
	"settings": "user://settings.json",
	"save": "user://save.json",
	"save_backup": "user://save_backup.json",
	"test": "user://test.json"
}


func save_value(file_name : String, key : String, value) -> void:
	if registered_files.has(file_name):
		save_to_file(registered_files[file_name], key, value)
	else:
		print_debug("There is no file with name " + file_name + ". Unable to save to.")


func load_value(file_name : String, key : String, default):
	if registered_files.has(file_name):
		var content = load_content(registered_files[file_name])
		if content.has(key):
			return content[key]
		else:
			return default
	else:
		print_debug("There is no file with name " + file_name + ". Unable to load from.")
		return default


func save_to_file(file : String, key : String, value) -> void:
	var parsed = load_content(file)
	parsed[key] = value
	var file_res = File.new()
	file_res.open(file, File.WRITE)
	file_res.store_line(JSON.print(parsed))
	file_res.close()


func load_content(file_path : String) -> JSONParseResult:
	# loading text from file
	var file = File.new()
	var content = ""
	if file.file_exists(file_path):
		file.open(file_path, File.READ)
		content = file.get_as_text()
		file.close()
	# return text parsed with json
	return json_parse(content)


func json_parse(content : String) -> JSONParseResult:
	if content == "":
		return JSON.parse("{}").result
	var data_parse = JSON.parse(content)
	if data_parse.error != OK:
		return JSON.parse("{}").result
	return data_parse.result


func clear_all_data() -> void:
	for file in registered_files:
		remove_file(file)


func remove_file(file_name : String) -> void:
	if not registered_files.has(file_name):
		print_debug("There is no file with name " + file_name + ". Unable to remove.")
		return
	var file_path = registered_files[file_name]
	var dir = Directory.new()
	var file = File.new()
	if file.file_exists(file_path):
		dir.remove(file_path)
